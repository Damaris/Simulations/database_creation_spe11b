'''
    Damaris example of using Ascent for in situ visualisation.
    
    Script to accept simulation mesh data and output png image of mesh 
    model using Ascent.
    
    This script is designed to be run by a Damaris server process that 
    is part of a simulation (in this case OPM Flow).


    pip install tensorflow-datasets
   
'''

def main(DD):
    # conduit + ascent imports
    import conduit
    import conduit.blueprint
    import ascent.mpi
    
    # to create a PyTorch dataset
    # import torch
    # from torch.utils.data import Dataset, DataLoader
    import os
    import shutil
    import math
    import numpy as np
    
    from damaris4py.server import listknownclients
    from damaris4py.dask import damaris_dask
    from damaris4py.server import getservercomm

    try:
        # These two dictionaries are set up in the PyAction constructor 
        # and are static.
        damaris_dict = DD['damaris_env']
        dask_dict    = DD['dask_env']
        # This third dictionary is set up in PyAction::PassDataToPython() and is 
        # typically different each iteration.
        iter_dict    = DD['iteration_data']   
        # This is the iteration value the from Damaris perspective
        # i.e. It is the number of times damaris_end_iteration() has been called
        it       = iter_dict['iteration']

        # There will be one key and corresponding NumPy array for each block of the variable
        # Each Damaris server can look after multiple client ranks
        client_list = listknownclients()      
        iteration    = damaris_dask.return_iteration(DD)
        
        
        # if it == 0:
        print('The DamarisData dictionaries are:')
        keys = list(DD.keys())
        print(keys)
        

        damaris_comm = getservercomm()
        
        # These are the variables that have been published to Python by the Damaris server process:
        print('The DamarisData variables available are :')
        keys = list(iter_dict.keys())
        for data_key in keys :
            if (data_key != 'iteration'):
                print('From Python,  Iteration: ', iteration, 'Variable name: ', data_key)
        
        print("From Python,  Iteration: ", iteration, "client_list: " , client_list) ;        
        total_sum = 0
        
        a = ascent.mpi.Ascent()
        
        
        ascent_opts = conduit.Node()
        ascent_opts["exeeptions"] = "forward"
        ascent_opts["mpi_comm"].set(damaris_comm.py2f())

        a.open(ascent_opts)
        
        multi_mesh_data = conduit.Node()
        
        for client_rank in client_list :
            # We know the variable names as they match what is in the Damaris XML file
            print('From Python,  Iteration: ', iteration)
            numpy_x = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/x', client_rank=client_rank )
            numpy_y = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/y', client_rank=client_rank )
            numpy_z = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/z', client_rank=client_rank )
            print("From Python , Iteration: ", iteration, "x, y, z: ", numpy_x[0], ", ", numpy_y[0], ", ",  numpy_z[0])
            
            numpy_connectivity = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/connectivity", client_rank=client_rank )
            numpy_offsets = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/offsets", client_rank=client_rank )
            numpy_types = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/types", client_rank=client_rank )
            
            # create container node
            mesh_data = conduit.Node()
            #  provide state information
            mesh_data["state/time"].set(iteration)
            mesh_data["state/cycle"].set(iteration)
            mesh_data["state/domain_id"] = client_rank


            # coordinate system data
            mesh_data["coordsets/coords/type"] = "explicit"
            mesh_data["coordsets/coords/values/x"].set_external(numpy_x)
            mesh_data["coordsets/coords/values/y"].set_external(numpy_y)
            mesh_data["coordsets/coords/values/z"].set_external(numpy_z)
            
            # topologies: 
            mesh_data["topologies/topo/type"] = "unstructured"
            mesh_data["topologies/topo/coordset"] = "coords"
            mesh_data["topologies/topo/elements/shape"] = "hex"
            mesh_data["topologies/topo/elements/connectivity"].set_external(numpy_connectivity) 
            
            # As we are creating the image from each MPI decomposition, we just need to provide 
            # a single Numpy array from each simulation client rank 
            numpy_PRESSURE = damaris_dask.return_numpy_array(DD, 'PRESSURE', client_rank=client_rank )
            
            # one or more scalar fields
            mesh_data["fields/pressure/type"]        = "scalar";
            mesh_data["fields/pressure/topology"]    = "topo";
            mesh_data["fields/pressure/association"] = "element";
            
            try:
                mesh_data["fields/pressure/values"].set_external(numpy_PRESSURE);
            except TypeError as err:    
                print('TypeError: No damaris data of name PRESSURE. Client rank: ', client_rank, '; Iteration: ' , iteration, '; Error: ', err)
            
            mesh_name = 'mesh_' + str(client_rank)
            multi_mesh_data[mesh_name] = mesh_data
            
            
        a.publish(multi_mesh_data);
        
        
        actions = conduit.Node()

        ## Failed attempt to create a threshold filter output
        # add_act2 = actions.append()
        # add_act2["action"] = "add_pipelines"
        # pipelines = add_act2["pipelines"]
         
        # # conduit::Node pipelines;
        # # pipelines conduit.Node() 
        # # pipeline 1
        # pipelines["pl1/f1/type"] = "threshold";
        # # filter knobs
        # # conduit::Node thresh_params = pipelines["pl1/f1/params"];
        # thresh_params = conduit.Node()
        # thresh_params = pipelines["pl1/f1/params"]
        # thresh_params["field"] = "pressure";
        # thresh_params["min_value"] = 2.5e7;
        # thresh_params["max_value"] = 3.0e7;
        
        add_act = actions.append()
        add_act["action"] = "add_scenes"
        
        scenes = add_act["scenes"]
        
        field = 'pressure'
        min_val = 1.0e7
        max_val = 2.5e7 
        image_width = 512 
        image_height = 128
        look_at_x = 4100.0
        look_at_y = 0.0
        look_at_z = 600.0
        position_x = 4100.0
        position_y = 8000.0
        position_z = 600.0
        up_x = 0.0
        up_y = 0.0
        up_z = -1.0
        fov = 45.
        azimuth = 0.0 
        elevation = 0.0
        zoom = 2.5
        scenes["s1/plots/p1/type"]  = "pseudocolor"
        scenes["s1/plots/p1/field"] = field
        scenes["s1/plots/p1/min_value"] = min_val ;
        scenes["s1/plots/p1/max_value"] = max_val ;
        scenes["s1/plots/p1/color_table/name"] = "Green";  # 
        
        scenes["s1/renders/r1/image_width"]  = image_width
        scenes["s1/renders/r1/image_height"] = image_height
        scenes["s1/renders/r1/bg_color"] =  [0.0, 0.0, 0.0] 
        scenes["s1/renders/r1/render_bg"] = 'true' 
        # scenes["s1/renders/r1/annotations"] = 'This is something interesting'
        
        
        mn = damaris_dask.return_magic_number(DD)
        ## The default_dir property does not seem to work as advertised
        #  scenes["default_dir"] = "/home/jbowden/tf_test_tfrecord/spe11b_test1/dummy_data"
        file_name = field + "_" + mn + "_ascent_iter_" + str(iteration)
        scenes["s1/renders/r1/image_name"]   = file_name

        scenes["s1/renders/r1/camera/look_at"] = [look_at_x, look_at_y, look_at_z]
        scenes["s1/renders/r1/camera/position"] = [position_x, position_y, position_z]
        scenes["s1/renders/r1/camera/up"] = [up_x, up_y, up_z]
        scenes["s1/renders/r1/camera/fov"] =  fov
        scenes["s1/renders/r1/camera/xpan"] = 0.
        scenes["s1/renders/r1/camera/ypan"] = 0.
        scenes["s1/renders/r1/camera/azimuth"] = 0.0
        scenes["s1/renders/r1/camera/elevation"] = 0.0
        scenes["s1/renders/r1/camera/zoom"] = zoom
        scenes["s1/renders/r1/world_annotations"] = "false"
        scenes["s1/renders/r1/color_bar_position"] = [1.,1., 1., 1.]  # set scale bar out of view
        
        a.execute(actions)
        a.close()
        
        import zipfile

        # Open a zip file at the given filepath. If it doesn't exist, create one.
        # If the directory does not exist, it fails with FileNotFoundError
        # DATABASE_FILENAME needs to be passed on the HyperQueue 'hq sumbit ...' command line
        # which is done using opm_runner.submitter.hq.py and the ENV_FOR_HYPERQUEUE variable 
        # in run_ascent.sh
        try:
            ensemble_str = os.environ['ENSEMBLE_MEMBER'] 
            file_extn = os.environ['DATADIRBN']
            if iteration == 0:
                import pandas as pd
                
                df = pd.read_csv('parameters.txt')
                df.rename( columns={'Unnamed: 0':'magic_number'}, inplace=True )
                df['magic_number'] = int(mn)
                df['field'] = field
                df['colour_scale_min'] = min_val
                df['colour_scale_max'] = max_val
                df['image_width'] = image_width
                df['image_height'] = image_height
                df['look_at_x'] = look_at_x
                df['look_at_y'] = look_at_y
                df['look_at_z'] = look_at_z
                df['position_x'] = position_x
                df['position_y'] = position_y
                df['position_z'] = position_z
                df['up_x'] = up_x
                df['up_y'] = up_y
                df['up_z'] = up_z
                df['fov'] = fov
                df['zoom'] = zoom
                # Rewrite output to local directory - this will be moved at end of each task via setup_and_run_spe11b.sh
                output_path='./parameters_'+file_extn+'_'+ensemble_str+'.csv'
                df.to_csv(output_path, mode='a', header=not os.path.exists(output_path))

        except KeyError as err:
            raise err        
        
    except KeyError as err: 
        print('KeyError: No damaris data of name: ', err)
    except PermissionError as err:
        print('PermissionError!: ', err)
    except ValueError as err:
        print('Damaris Data is read only!: ', err)
    except UnboundLocalError as err:
        print('Damaris data not assigned!: ', err)   
    finally:
        pass


if __name__ == '__main__':
    main(DamarisData)

