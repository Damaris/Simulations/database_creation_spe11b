#!/bin/bash
# This is the HyperQueue job that is run for each OPM model realisation requested
date
echo PWD=$PWD


echo "Generating SPE11B case from $@"
echo "pyopmspe11 -i $@ -m deck -o SPE11B"
pyopmspe11 -i $@ -m deck -o SPE11B 


INPUTCASE=$(realpath SPE11B/deck/SPE11B.DATA)
# Make sure the above setup has worked
if [[ "$?" != "0" ]] ; then
   exit -1 
fi 
echo "running flow with $INPUTCASE"

# Currently has no effect on number of OPM threads used by simulation
export OPM_NUM_THREADS=3
echo "HQ_CPUS = $HQ_CPUS"

unset FLOW_DAMARIS_XML_FILE
echo "ACROSS_PUBLISH_DATA_SCRIPT=${ACROSS_PUBLISH_DATA_SCRIPT}"

# ensemble_number=$(get_ensemble_number.py)
export ENSEMBLE_MEMBER=${HQ_JOB_ID}_${HQ_TASK_ID}_${HQ_INSTANCE_ID}
echo "ENSEMBLE_MEMBER=$ENSEMBLE_MEMBER (the ensemble number)"

# Glob for the previously failed run png files
PNGFILES=( ./*.png )
if [[ ${#PNGFILES[@]} -gt 0 ]] ; then
  # Delete previous png files in task directory as it seems we have started this task again.
  rm  *.png
fi 

# And now we can run flow  --map-by slot:pe=${SLOTPE}   --cpu-set $HQ_CPUS --bind-to core
echo mpirun -np "${FAKE_ERT_NUM_PROCS:-2}" --cpu-set $HQ_CPUS  --bind-to core --report-bindings flow \
    --threads-per-process=$OPM_NUM_THREADS \
    --enable-damaris-output=true \
    --enable-ecl-output=false \
    --damaris-python-script=$ACROSS_PUBLISH_DATA_SCRIPT \
    --damaris-dedicated-cores=1 \
    --damaris-sim-name=flow-$ENSEMBLE_MEMBER \
    --damaris-save-to-hdf=false \
    --damaris-log-level=debug \
    --enable-vtk-output=false \
    "$INPUTCASE"
mpirun -np "${FAKE_ERT_NUM_PROCS:-2}"   --cpu-set $HQ_CPUS --bind-to core  --report-bindings flow \
    --threads-per-process=$OPM_NUM_THREADS \
    --enable-damaris-output=true \
    --enable-ecl-output=false \
    --damaris-python-script=$ACROSS_PUBLISH_DATA_SCRIPT \
    --damaris-dedicated-cores=1 \
    --damaris-sim-name=flow-$ENSEMBLE_MEMBER \
    --damaris-save-to-hdf=false \
    --damaris-log-level=debug \
    --enable-vtk-output=false \
    "$INPUTCASE"

echo PWD=$PWD
echo "DATABASE_DIR=$DATABASE_DIR"
echo "DATABASE_DIR/DATADIRBN=${DATABASE_DIR}/${DATADIRBN}"
# Move log files to local directory
mv SPE11B/deck/damaris_log/*.log .

# Tar together all output PNG files
tar -czf SPE11B_${ENSEMBLE_MEMBER}_png.tar.gz ./*.png
sleep 1
cp SPE11B_${ENSEMBLE_MEMBER}_png.tar.gz ${DATABASE_DIR}/${DATADIRBN}
sleep 1
# output_path='./parameters_'+file_extn+'_'+ensemble_str+'.csv'
cp ./parameters_${DATADIRBN}_${ENSEMBLE_MEMBER}.csv ${DATABASE_DIR}/${DATADIRBN}

# Delete temporaty files - removing this in devlopment version
# # Glob for the .stdout files
# files=( ./flow*.stdout )
# export GLOBIGNORE=*.stderr:*.stdout:*.log
# # Check that the number of stdout files is greater than 0
# if [[ ${#files[@]} -gt 0 ]] ; then
  # # Delete all outputs (except log/stderr/stdout files)
  # rm -rv *
# fi 
# unset GLOBIGNORE

date