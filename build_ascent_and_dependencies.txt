#! /bin/bash
# 
# Installation of VTKm, Conduit and Ascent on Linux
#
# Josh Bowden (Inria). 
# 
# Directions for install of all dependencies is available on the [Ascent website](https://ascent.readthedocs.io/en/latest/BuildingAscent.html).
# Based on https://github.com/Alpine-DAV/ascent/blob/develop/scripts/build_ascent/build_ascent.sh
#
# Assumes HDF5 and MPI is installed
# hdf5_dir= is needed for Conduit
# root_dir=<where you want to put the install files>
# install_dir=<where we want to install the libraries>
#
# Once installed add: PYTHONPATH=$install_dir:$install_dir/python-modules:$PYTHONPATH

root_dir=/mnt/proj1/dd-23-66/ascent_install
install_dir=/mnt/proj1/dd-23-66/local
build_jobs=8
hdf5_dir=$EBROOTHDF5


if [[ ! -d $root_dir ]] ; then
  mkdir -p $root_dir
fi 
cd $root_dir

if [[ ! -d $install_dir ]] ; then
  mkdir -p $install_dir
fi 
python_modules=$install_dir



####################
# VTK-m
####################
# vtkm_version=v1.9.0
vtkm_version=v2.1.0
vtkm_src_dir=${root_dir}/vtk-m-${vtkm_version}
vtkm_build_dir=${root_dir}/build/vtk-m-${vtkm_version}
vtkm_install_dir=${install_dir}
vtkm_tarball=vtk-m-${vtkm_version}.tar.gz

# if ${build_vtkm}; then
if [ ! -d ${vtkm_src_dir} ]; then
  echo "**** Downloading ${vtkm_tarball}"
  curl -L https://gitlab.kitware.com/vtk/vtk-m/-/archive/${vtkm_version}/${vtkm_tarball} -o ${vtkm_tarball}
  tar -xzf ${vtkm_tarball}
fi

mkdir -p $vtkm_build_dir
cd $vtkm_build_dir


#     -DVTKm_NO_DEPRECATED_VIRTUAL=ON 
cmake -S ${vtkm_src_dir} -B ${vtkm_build_dir} \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON\
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_SHARED_LIBS=ON\
  -DVTKm_USE_DOUBLE_PRECISION=ON \
  -DVTKm_USE_64BIT_IDS=OFF  \
  -DVTKm_USE_DEFAULT_TYPES_FOR_ASCENT=ON \
  -DVTKm_ENABLE_MPI=ON \
  -DVTKm_ENABLE_OPENMP=ON \
  -DVTKm_ENABLE_RENDERING=ON \
  -DVTKm_ENABLE_TESTING=OFF \
  -DBUILD_TESTING=OFF \
  -DVTKm_ENABLE_BENCHMARKS=OFF \
  -DVTKm_INSTALL_EXAMPLES=ON \
  -DVTKm_Vectorization=native \
  -DCMAKE_INSTALL_PREFIX=${install_dir} 
  

echo "**** Building VTK-m ${vtkm_version}"
cmake --build ${vtkm_build_dir} -j${build_jobs}
echo "**** Installing VTK-m ${vtkm_version}"
cmake --install ${vtkm_build_dir}

####################
# Conduit
####################
# On Debian/Ubuntu: hdf5_dir=/usr/lib/x86_64-linux-gnu/hdf5/openmpi

conduit_install_dir=${install_dir}
conduit_src_dir=${root_dir}/conduit/src
conduit_build_dir=${root_dir}/build/conduit

cd ${root_dir}
git clone --recursive https://github.com/llnl/conduit.git
cd conduit
git submodule init
git submodule update

cd ..
mkdir -p ${conduit_build_dir}
cd ${conduit_build_dir}
cmake -S ${conduit_src_dir} -B ${conduit_build_dir} \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_SHARED_LIBS=ON \
  -DCMAKE_INSTALL_PREFIX=${install_dir} \
  -DENABLE_MPI=ON \
  -DENABLE_FIND_MPI=ON \
  -DENABLE_EXAMPLES=ON \
  -DENABLE_UTILS=ON \
  -DENABLE_PYTHON=ON \
  -DHDF5_DIR=${hdf5_dir} \
  -DENABLE_TESTS=OFF \
  -DPYTHON_MODULE_INSTALL_PREFIX=${python_modules}
  
  
  
#############################
# Ascent
####################
cd ${root_dir}
git clone --recursive https://github.com/Alpine-DAV/ascent.git
cd ascent
git submodule init
git submodule update

ascent_src_dir=${root_dir}/ascent/src
ascent_build_dir=${root_dir}/build/ascent
mkdir -p ${ascent_build_dir}
cd ${ascent_build_dir}

MPIEXEC=$(which mpiexec)
PYTHON3EXE=$(which python)

echo "**** Configuring Ascent"
cmake -S ${ascent_src_dir} -B ${ascent_build_dir} \
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_SHARED_LINKER_FLAGS_RELEASE="-L${install_dir}/lib" \
  -DBUILD_SHARED_LIBS=ON \
  -DCMAKE_INSTALL_PREFIX=${install_dir} \
  -DEXAMPLE_OUTPUT_DIRECTORY=${install_dir}/examples/ascent \
  -DENABLE_MPI=ON \
  -DMPI_EXECUTABLE=$MPIEXEC \
  -DVTKm_Vectorization=native \
  -DENABLE_OPENMP=ON \
  -DENABLE_FIND_MPI=ON \
  -DENABLE_FORTRAN=OFF \
  -DENABLE_VALGRID=OFF \
  -DENABLE_TESTS=OFF \
  -DENABLE_PYTHON=ON \
  -DPYTHON_EXECUTABLE=$PYTHON3EXE \
  -DBLT_CXX_STD=c++14 \
  -DCONDUIT_DIR=${conduit_install_dir} \
  -DVTKM_DIR=${vtkm_install_dir} \
  -DENABLE_VTKH=ON 
  
  # -DCAMP_DIR=${camp_install_dir} \
  # -DRAJA_DIR=${raja_install_dir} \
  # -DUMPIRE_DIR=${umpire_install_dir} \
  # -DMFEM_DIR=${mfem_install_dir} \
  # -DENABLE_APCOMP=ON \
  # -DENABLE_DRAY=ON
  
ASCENTPATH=${install_dir}:${install_dir}/python-modules
export PYTHONPATH=$ASCENTPATH:$PYTHONPATH  