# Workflow for simulation image Database creation
 This repository is designed to create an image database for the [SPE11B simulation model](https://opm-project.org/?p=1590)  
 
 Date: 20/02/2024  
 Project: ACROSS (EuroHPC JU funded)  
 Authors: Joshua Bowden (Inria), Kjetil Olsen Lye (SINTEF)  
  
 Download:  
```bash
git clone https://gitlab.inria.fr/Damaris/Simulations/database_creation_spe11b.git
cd  database_creation_spe11b
```
 
 This workflow repository is based on code from:  
 https://github.com/kjetilly/opm-runner.git
 and
 https://github.com/kjetilly/across_review/tree/spe11
  
##  Description: 
   ```run_ascent_workflow.sh``` runs an ensemble of OPM flow simulations, via HyperQueue specifically for the SPE11B case.  
   The input to the simulations is varied using the opm-runner based code, and the available templated input file 
   (```spe11b_inject.tmpl```) and its associated inputs:  
          PARAMETERSFILENAME=parameters/parameters_ascent.txt  
          ERTFILENAME=$REPODIR/spe11/spe11.ert  
  
   The script ```ACROSS_PUBLISH_DATA_SCRIPT=ascent_mesh_conduit.py``` is called via Damaris on each output iteration.  
   Output time and timestep are defined at bottom of the file ```template/spe11b_inject.tmpl```.  
   The code creates an output PNG file image and saves it to the working directory.  
   At the end of a simulation instance all images ar consolidated to a tar.gz file and moved  
   to the database directory (defined in ```DATABASE_FILENAME```).  
   This directory will also contain the parameters used to create each realisation.  
     
## Required software:
   - Damaris with Python support:  
       see: https://gitlab.inria.fr/Damaris/damaris/-/wikis/Damaris-Python-Support  
   - OPM flow with Damaris support:  
       see: https://opm-project.org/?page_id=231  
       and add -DUSE_DAMARIS_LIB=TRUE to CMake options of opm-simulators build)  
       Other dependencies of OPM flow can be built using a EasyBuild recipie that is  
       designed for installation on the IT4I Karolina supercomputer.
       available here: https://git.mycloud-links.com/across/wp7/karolina_easybuild_env
   - The pyopmspe11 Python library: 
        git clone https://github.com/kjetilly/pyopmspe11)  
   - HyperQueue, running on a SLURM based cluster  
        HQ also supports PBS, this would need to be changed in the ```run_ascent_workflow.sh``` script  
   - Ascent installed (see file: build_ascent_and_dependencies.txt)  

## Install and run the workflow
```bash
# Install and run:
# Download the git repository:
git clone https://gitlab.inria.fr/Damaris/Simulations/database_creation_spe11b.git  
# Change to the repository root directory
cd  database_creation_spe11b
# set up modules to load and paths to installed components
nano source_env.sh
# Modify the input variables to suit the system being run on
nano run_ascent_workflow.sh  
# e.g.
# Set the once-off paths
#    Set the path to the root directory of this repository (database_creation_spe11b):
#    REPODIR=
#    # Set a temprary (scratch) directory for simulation results
#    OUTPUTDIR=
#    # Set the directory of where created database files will be stored (data or project space)
#    DATABASE_DIR=
# 
# Now set per-job variables:
#    DATADIRBN=v001e_10SIMS_10YINJECT_100YWAIT
# The number of simulations to run and random number generator seed
#    NUMREALISATIONS=10
#    SEED=42
#
# Some HyperQueue job paramaters for SLURM based worker
#    HQ_IDLE_TO=5m
# Use the following to allow extra workers to run:
#    HQ_MAX_WORKERS=2
# Workers per alloc seems to need to be 1 for both
# queues 'qcpu_exp' and 'qcpu'
#    HQ_WORKERS_PER_ALLOC=1
# Change this to match the number of cores needed by a simulation rank 
#(equivalent to the OpenMP thread count?)
#    HQ_CPUS_PER_SAMPLE=4
# Use this to set the distribution of slots on a worker
# --cpus 32x4  => 32 NUMA regions of 4 cores each 
#    HQ_WORKER_CPUS_PER_TASK=32x4
#
# The template file - Edit this file to set the simulation time scale and how often to output data
#    ERT_TEMPLATE_FILE=template/spe11b_inject.tmpl
#
# Run the workflow:
# From the repository root directory:
# Use nohup as the script does not return until all jobs have run (could change this to use: hq job wait [job_number])
nohup ./run_ascent_workflow.sh &
```
