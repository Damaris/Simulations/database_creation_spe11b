#! /bin/bash

# Load modules and environment specific paths here:
# source this if not already in environment: source /mnt/proj1/dd-23-66/source.sh

DAMARIS_PATH=/home/jbowden/local:/mnt/proj1/dd-23-66/local
export LD_LIBRARY_PATH=$DAMARIS_PATH/lib:$DAMARIS_PATH/lib64:$LD_LIBRARY_PATH
export PATH=$DAMARIS_PATH/bin:$DAMARIS_PATH/../.local/bin:$PATH

# Python paths
ASCENT_PYTHON_PATH=/mnt/proj1/dd-23-66/local:/mnt/proj1/dd-23-66/local/python-modules
DAMARIS_PYTHON_DIR=$HOME/mypylib:$(python -m site --user-site)
export PYTHONPATH=$DAMARIS_PYTHON_DIR:$ASCENT_PYTHON_PATH:/apps/all/SciPy-bundle/2022.05-foss-2022a/lib/python3.10/site-packages