#! /bin/bash
# Script: run_ascent_workflow.sh
# Date: 20/02/2024
# Project: ACROSS (EuroHPC JU funded)
# Author: Joshua Bowden (Inria), Kjetil Olsen Lye (SINTEF)
# Description:
#   Runs an ensemble of OPM flow simulations (NUMREALISATIONS), 
#   via HyperQueue specifically for the SPE11B case.
#
# Usage:
# # use nohup as the script does not return until all jobs have run
# $ nohup ./run_ascent_workflow.sh &

# ... Start of user modifiable paramaters ... #  

########################################################
# ...  One time variables setup  ... #
# Add the path to where we have installed the repository.
# N.B. It is also where we have to run 'run_ascent.sh' from, as paths to the 
# spe11b data set files are relative to here.
REPODIR=/scratch/project/dd-23-66/cae_workflow/database_creation_spe11b
# This is temporay output area where each simulation is run
OUTPUTDIR=/scratch/project/dd-23-66/cae_workflow/outputdir
# Base directory of where created database files will be stored
DATABASE_DIR=/mnt/proj1/dd-23-66/SPE11B_DB
# Set up paths to flow executable and libraries
source source_env.sh

########################################################
# ... Per execution variables ... #
# Set these to appropriate values for your database creation

export DATADIRBN=v001i_32SIMS_5Y_INJECT_50Y_WAIT_PNG_ONLY
NUMREALISATIONS=32
SEED=42

# This is used in the setup_and_run_spe11b.sh execution script, so make sure it
# is defined before the HQ SLURM job is launched so it becomes part of the
# execution environment. It is passed to flow using --damaris-use-python="filename"
export ACROSS_PUBLISH_DATA_SCRIPT=$REPODIR/ascent_mesh_conduit.py


###
# Some HyperQueue job paramaters for SLURM based worker
# It is important that individual simulations do not take longer than TIMELIMIT
# which is the time available in the SLURM allocation. As when they
# fail due to walltime limit HyperQueue will alloacte another SLURM job and relaunch
# the same simulations, thus continuing the cycle of failure.
TIMELIMIT=1h
TIMELIMIT_WORKER=1h
QUEUE=qcpu_exp
QUEUE=qcpu
ACCOUNT=dd-23-66

# These variables are available if neeeded.
HQ_IDLE_TO=5m
# Use the following to allow extra workers to run:
HQ_MAX_WORKERS=1
# Workers per alloc seems to need to be 1 for both
# queues 'qcpu_exp' and 'qcpu'
HQ_WORKERS_PER_ALLOC=1
# Change this to match the number of cores needed by a simulation rank 
# (equivalent to the OpenMP thread count?)
HQ_CPUS_PER_SAMPLE=4
# Use this to set the distribution of slots on a worker
# --cpus 32x4  => 32 NUMA regions of 4 cores each 
HQ_WORKER_CPUS_PER_TASK=32x4



###
# Extra input data for files for OPM flow case
# Input files PARAMETERS and ERTFILE are specified in-line below.
# These three files will be saved to the database output area.
PARAMETERSFILENAME=parameters/parameters_ascent.txt
ERTFILENAME=$REPODIR/spe11/spe11.ert
# ERT_TEMPLATE_FILE is where the simulation time line and output schedule is specified
ERT_TEMPLATE_FILE=template/spe11b_inject.tmpl


PARAMETERS="$(cat <<-EOF
TOPPRESSURE UNIFORM 19420000 19820000 
ROCKSPECIFICHEAT UNIFORM 8.2e-1 8.7e-1
ROCKDENSITY UNIFORM 2300 2700
BOTTOMRIGTEMPERATURE UNIFORM 60 80 
TOPRIGTEMPERATURE UNIFORM 33 40
INJECTRATE1 UNIFORM 0.0 0.05
INJECTRATE2 UNIFORM 0.0 0.05
EOF
)"

CASEDIRNAME=spe11

# ERTFILE lines will be saved to ERTFILENAME
ERTFILE="$(cat <<-EOF
NUM_REALIZATIONS $NUMREALISATIONS
QUEUE_SYSTEM HQ

-- Output paths output/simulations/runpath/realization-%d/iter-%d
RUNPATH output/realization-%d/iter-%d
ENSPATH output/storage

-- case names
-- The directory "spe11" must exist in the output directory (im not sure why)
ECLBASE EXAMPLE%d
DATA_FILE $CASEDIRNAME/spe11b.txt

-- this set ups the random variables to be used in the simulation
GEN_KW MULT_PORO $ERT_TEMPLATE_FILE spe11b.txt $PARAMETERSFILENAME
RANDOM_SEED ${SEED}
INSTALL_JOB flow FLOW
SIMULATION_JOB flow <ECLBASE>

SUMMARY W*
SUMMARY F*
SUMMARY BPR*
EOF
)"

# A list of environment variables to be exposed to the "hq submit ..." task
# The variable names added here must be exported somewhere in the current script.
# They are used in file installdir\src\opm-runner\opm_runner\submitter\hq.py
export ENV_FOR_HYPERQUEUE="ACROSS_PUBLISH_DATA_SCRIPT:DATABASE_DIR:DATADIRBN:PYTHONPATH"
######################################################

# ... End of user modifiable paramaters ... #

######################################################
# Create the input files
echo "$PARAMETERS" > $CASEDIRNAME/$PARAMETERSFILENAME
echo "$ERTFILE" > $ERTFILENAME

#####################################################
# Add out repository Python module paths
export PYTHONPATH=$REPODIR/installdir/src/opm-runner:$REPODIR/installdir/src/opm-runner/opm-runner:$PYTHONPATH

################################
# derived variables
# HQ_TASK_SCRIPT is the shell script that executes  the OPM flow simulation
HQ_TASK_SCRIPT=$REPODIR/installdir/bin/setup_and_run_spe11b.sh

# We will use this environment variable in the Ascent python script 
# (ascent_mesh_conduit.py) as path to project storage
export DATABASE_DIR=$DATABASE_DIR

DATADIR=$OUTPUTDIR/$DATADIRBN
DBOUTPUT=$DATABASE_DIR/$DATADIRBN
################################

# Make sure this is executable:
chmod u+x $REPODIR/installdir/bin/setup_and_run_spe11b.sh

# Make sure of directories exist
# Create the output database directory - make sure it does not exist already
if [[ ! -d "$DBOUTPUT" ]] ; then
  mkdir -p "$DBOUTPUT"
else
  echo "Error: The database output directory exists: $DBOUTPUT "
  echo "  Perhaps the input DATADIRBN variable has been used previously? Please choose another"
  exit -1
fi
echo "Moving input files to DB directory:"
echo "    cp $CASEDIRNAME/$PARAMETERSFILENAME $DATABASE_DIR/$DATADIRBN"
echo "    cp $ERTFILENAME $DATABASE_DIR/$DATADIRBN"
echo "    cp $CASEDIRNAME/$ERT_TEMPLATE_FILE $DATABASE_DIR/$DATADIRBN"
cp "$CASEDIRNAME/$PARAMETERSFILENAME" "$DATABASE_DIR/$DATADIRBN"
cp "$ERTFILENAME" "$DATABASE_DIR/$DATADIRBN"
cp "$CASEDIRNAME/$ERT_TEMPLATE_FILE" "$DATABASE_DIR/$DATADIRBN"

# Make sure temporary output directories exist
if [[ ! -d $OUTPUTDIR/$CASEDIRNAME ]] ; then
  mkdir -p $OUTPUTDIR/$CASEDIRNAME
fi
cd $OUTPUTDIR

# Save a script that will allow us to finalise the data 
# in the database incase of failure
FINALISEDATA="$(cat <<-EOF
DATADIR=$DATADIR
DBOUTPUT=$DATABASE_DIR/$DATADIRBN
DATADIRBN=$DATADIRBN
NUMREALISATIONS=$NUMREALISATIONS

# coordinated with processing done at end of each job/task
echo "$0 Moving and consolidating database files to: \$DBOUTPUT" 

# some statistics of interest
# The OPM flow simulation walltimes
if [[ ! -f \$DBOUTPUT/opm_timings.txt ]] ; then
    find \$DATADIR -name "flow*.stdout" | xargs grep -Rh -e "Simulation time"  > \$DBOUTPUT/opm_timings.txt
    echo "Simulation times: opm_timings.txt"
fi

# The Damaris server free time
if [[ ! -f \$DBOUTPUT/damaris_timings.txt ]] ; then
    find \$DATADIR -name "*.log" | xargs grep -Rh -e "TIME:"  > \$DBOUTPUT/damaris_timings.txt
    echo "Damaris server iteration times: damaris_timings.txt"
    cat \$DBOUTPUT/damaris_timings.txt | grep  "magic_number" | sed 's|^.*TIME: ||g' | uniq > \$DBOUTPUT/damaris_timing_b.txt
    cat \$DBOUTPUT/damaris_timings.txt | grep -v "magic_number" | sed 's|^.*TIME: ||g' >> \$DBOUTPUT/damaris_timing_b.txt
    mv \$DBOUTPUT/damaris_timing_b.txt \$DBOUTPUT/damaris_timings.txt
fi

# Place header into output file:
if [[ ! -f \$DBOUTPUT/all_parameters.out ]] ; then
    find \$DBOUTPUT -name "*parameters_*.csv"  | head -n 1 | xargs sed -n '1p'  > \$DBOUTPUT/all_parameters.out
    # Place data into same file
    find \$DBOUTPUT -name "parameters_*.csv"  -exec sed -n '2p' {}  \; >> \$DBOUTPUT/all_parameters.out
    LINECOUNT=\$(cat \$DBOUTPUT/all_parameters.out | wc -l)
    NUMREALISATIONSPLUS1=\$(( \$NUMREALISATIONS + 1 ))

    if [[ "\$LINECOUNT" = "\$NUMREALISATIONSPLUS1" ]] ; then
        echo "Per simulation paramters are gathered to file: all_parameters.out"
        rm \$DBOUTPUT/parameters_*.csv
    fi
fi

# Tar and remove each simulations image file collection.
if [[ ! -f \$DBOUTPUT/SPE11B_\$DATADIRBN.tar ]] ; then
    tar -cvf SPE11B_\$DATADIRBN.tar *.tar.gz 
    if [[ \$? -eq 0 ]] ; then
        echo "You will find the images in: SPE11B_\$DATADIRBN.tar"
        rm  \$DBOUTPUT/*.tar.gz
    fi
fi

EOF
)"
echo "$FINALISEDATA" > $DBOUTPUT/finalise_data.sh
chmod u+x $DBOUTPUT/finalise_data.sh


# Test if HyperQueue server is running
hq server info
RUNNING=$?
if [[  "$RUNNING" == "1" ]] ; then
    echo "Starting the HQ server"
    hq server start > hqserver.out 2> hqserver.err < /dev/null &
    sleep 5 # give it time to start before we go on
fi

# Check if HyperQue worker is present
HQWORKERPRESENT=$(hq worker list | grep SLURM | awk -F "|" '{print $7 }')
if [[ -z "$HQWORKERPRESENT" ]] ; then
    echo "Starting HQ workers"
    # --cpus 32x4  => 32 NUMA regions of 4 cores each  --cpus 16x8
    # increase the number of --workers-per-alloc to get more nodes
    # --worker-time-limit  
    # N.B. In hq submit (see hq.py) can add:  --max-fails 1 then if a 
    #      job fails all are canceled.
    echo "... hq alloc add slurm ... "
    hq alloc add slurm --worker-time-limit $TIMELIMIT_WORKER \
                       --time-limit $TIMELIMIT \
                       --idle-timeout $HQ_IDLE_TO \
                       --max-worker-count $HQ_MAX_WORKERS \
                       --workers-per-alloc $HQ_WORKERS_PER_ALLOC \
                       --cpus $HQ_WORKER_CPUS_PER_TASK  \
                       -- -p $QUEUE -A $ACCOUNT --export=ALL 
fi

sleep 2
echo ""
date
echo "Starting fake-ERT scripts"
echo "    Calling: python run_ensemble_from_ert.py" 
echo "                 --outputdir $DATADIR"
echo "                 --ert-file $ERTFILENAME"
echo "                 --flowpath $HQ_TASK_SCRIPT"
echo "                 --cpus-per-sample $HQ_CPUS_PER_SAMPLE"
echo ""
echo "    If it fails for some reason, but simulations have completed, then finalise data by running:"
echo "    $DBOUTPUT/finalise_data.sh"
echo ""

echo "    DATADIR=$DATADIR"
echo "    DBOUTPUT=$DATABASE_DIR/$DATADIRBN"
echo "    DATADIRBN=$DATADIRBN"
echo ""
python \
    $REPODIR/installdir/src/opm-runner/bin/run_ensemble_from_ert.py \
    --outputdir $DATADIR \
    --ert-file $ERTFILENAME \
    --flowpath $HQ_TASK_SCRIPT \
    --cpus-per-sample $HQ_CPUS_PER_SAMPLE
RES=$?

echo "Finished call to run_ensemble_from_ert.py" 

if [[ $RES -ne 0 ]] ; then
    echo ""
    date
    echo "fake-ERT script seemed to fail after running run_ensemble_from_ert.py - exiting"
    echo "Run the finalisation script available: $DBOUTPUT/finalise_data.sh"
    echo "To stop the HyperQueue server, use: hq server stop"
    exit -1 
fi
echo "Finished fake-ERT scripts"

cd $OLDPWD

cd $DBOUTPUT
echo ""
echo "Running finalisation script: $DBOUTPUT/finalise_data.sh"
./finalise_data.sh

echo ""
echo "Please remove temporary files from here: $DATADIR"
echo "To stop the HyperQueue server, use: hq server stop"

cd $OLDPWD

date

